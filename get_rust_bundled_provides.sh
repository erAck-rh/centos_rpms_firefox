#!/bin/bash
# Get the list of the compiled bundled rust crates.
# Usage: $0 build.log

if [ -z $1 ]; then
  echo Get the list of the compiled bundled rust crates.
  echo Usage: $0 build.log
  exit 
fi
while read LINE; do
  name=`echo $LINE | cut -d\  -f1`
  version=`echo $LINE | cut -d\  -f2|sed -e 's/^v//g'`
  echo "Provides: bundled(crate($name)) = $version"
done < <(cat $1 |grep "[0-9]*[ ]*Compiling [a-z]"|sed -e 's/.*Compiling //'g| sort|uniq)
